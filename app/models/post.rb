class LinkValidator < ActiveModel::Validator
  def validate(record)

    #format = "https://"
  end
end

class Post < ApplicationRecord
  validates :title, presence: true, length: { minimum: 5 }
  validates :author, presence: true, length: { minimum: 5 }
  validates :description, presence: true, length: { minimum: 30, maximum: 400 }
  validates :image, :link, format: { with: /[http:]/, message: "filled in incorrectly" }

  validates_with LinkValidator, fields: [:author]
end
