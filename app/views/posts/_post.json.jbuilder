json.extract! post, :id, :title, :author, :image, :description, :link, :created_at, :updated_at
json.url post_url(post, format: :json)
